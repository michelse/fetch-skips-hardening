#! /usr/bin/env python3

usage = """
fault.py -- Fault injection campaign main script.

This script runs a single fault injection campaign on a reference or hardened
program. A campaign consists of injecting a kind of fault (eg. S32(1), S&R32,
random multi-fault...) at a set number of control points in the targeted
program (either exhaustively all PC values or random short intervals) and
collecting execution results.

usage: fault.py --campaign=CAMPAIGN [--block-wall]
                -d <FOLDER> -n <NAME> [--qemu=<PATH>]
                <INPUT>

Campaign selection with --campaign:
  ref-ex-s32-1:     Exhaustive S32(1)  (reference binary)
  ref-ex-s32-2:     Exhaustive S32(2)  (reference binary)
  ref-ex-sar32:     Exhaustive S&R32   (reference binary)
  fsh-ex-s32-1:     Exhaustive S32(1)  (hardened binary)
  fsh-ex-s32-2:     Exhaustive S32(2)  (hardened binary)
  fsh-ex-sar32:     Exhaustive S&R32   (hardened binary)
  fsh-multi-random: Random-location injections of random multi-fault sequences.

For reference campaigns (ref/...) a non-hardened binary is assumed. The
injection area is delimited by the __user_start and __user_end symbols in the
ELF. For hardened campaigns (fsh/...) a hardened binary is assumed. The
injection area is delimited by the __ccs_start and __ccs_end symbols.

Simulation options:
  --block-wall:     Raise the CCS_BYPASSED exception upon reaching the end of a
                    block where a fault was injected. Default is false for
                    reference binaries. Always on for hardened binaries.

File management options:
  -d <FOLDER>:      Folder in which statistics/logs should be output
  -n <NAME>:        Unique basename for this program within <FOLDER>
  --qemu=<PATH>:    Path to custom QEMU binary or wrapper script

The command to execute is specified via an <INPUT> folder. The exact binary and
arguments to run are obtained through make rules faultcmd_REF and faultcmd_FSH
in that folder. In the command printed by these rules, the binary should come
first because this script opens it to find ELF sections and symbols.
"""

import os
import sys
import enum
import shlex
import getopt
import random
import subprocess
import elftools.elf.elffile

Result = enum.Enum("Result", [
    # Program exited normally (shouln't happen with block wall)
    "EXITED",
    # The countermeasure detected invalid checksums, jumps, etc.
    "CCS_VIOLATION",
    # The countermeasure was bypassed (block wall reached)
    "CCS_BYPASSED",
    # The faulted address wasn't reached (null result)
    "NOT_REACHED",
    # The fault was S&R32 and the repeated word is equal to the original one
    "SILENT_REPLACE",
    # Signals
    "SIGSEGV", "SIGILL", "SIGTRAP",
    # Anything else (logged in comment in the output file)
    "OTHER"])

class TestSet:
    def __init__(self, title, start, end):
        self.title = title
        self.total = {r: 0 for r in Result}
        self.start = start
        self.end = end
        self.next = None
        self.reported = []
        self.report_bypasses = True
        self.loaded_str = ""
        self.finished = False

        self.autosave = None
        self.AUTOSAVE_FREQ = 20
        self.save_counter = 0

    def set_report_bypasses(self, b):
        self.report_bypasses = b

    def set_autosave(self, file):
        self.autosave = file

    def register(self, r, fault_description):
        if isinstance(r, tuple):
            fault_description = str(fault_description) + ":\n" + str(r[1])
            r = r[0]
        self.total[r] += 1
        if r == Result.EXITED or r == Result.OTHER:
            self.reported.append((r, fault_description))
        if self.report_bypasses and r == Result.CCS_BYPASSED:
            self.reported.append((r, fault_description))

        if self.autosave is not None:
            self.save_counter += 1
            if self.save_counter >= self.AUTOSAVE_FREQ:
                with open(self.autosave, "w") as fp:
                    self.print_to_file(fp, False)
                self.save_counter = 0

    def load_from_file(self, fp):
        l1 = fp.readline()
        l2 = fp.readline().strip().split(",")
        l3 = fp.readline().strip().split(",")
        if l1[0] not in "@=" or len(l2) != 10 or len(l3) != 10 or \
           l2[0] != "setting" or l3[0] != self.title:
            return

        self.finished = (l1[0] == '=')

        # Reload progress
        self.next = int(l1[1:].strip())
        for (r_str, total) in zip(l2[1:], l3[1:]):
            r = next(r for r in Result if r.name == r_str)
            self.total[r] = int(total)

        # Reload other comments
        self.loaded_str = fp.read()

    def print_to_file(self, fp, finished):
        fp.write(f"{'=' if finished else '@'} {self.next}\n")
        fp.write("setting")
        for r in Result:
            fp.write(f",{r.name}")
        fp.write("\n")
        fp.write(self.title)
        for r in Result:
            fp.write(f",{self.total[r]}")
        fp.write("\n")
        for (r, fault_description) in self.reported:
            text = f"{r.name} for {fault_description}"
            text = "\n".join(("# " + x).strip() for x in text.splitlines())
            fp.write(text + "\n")
        fp.write("\n")
        fp.write(self.loaded_str.strip() + "\n")

# Run a QEMU command with faults by inserting arguments. Returns a Result enum.
def run_with_faults(command, faults, block_wall):
    cmd = command[:]
    if len(faults):
        cmd.insert(1, "--riscv-faults")
        cmd.insert(2, ";".join(faults))
    if block_wall:
        cmd.insert(1 + 2 * (len(faults) > 0), "--riscv-faults-block-wall")
    try:
        p = subprocess.run(cmd, stdout=subprocess.DEVNULL,
            stderr=subprocess.PIPE, timeout=5)
        rc = p.returncode
    except subprocess.TimeoutExpired:
        return (Result.OTHER, "timeout")

    if b"qemu: unhandled CPU exception 0x18" in p.stderr:
        assert rc == 1
        return Result.CCS_VIOLATION
    elif b"qemu: unhandled CPU exception 0x19" in p.stderr:
        assert rc == 1
        return Result.CCS_BYPASSED
    elif rc == 132:
        return Result.SIGILL
    elif rc == 133:
        return Result.SIGTRAP
    elif rc == 139:
        return Result.SIGSEGV
    elif rc == 0:
        return Result.EXITED
    else:
        return (Result.OTHER, p.stderr.decode("utf-8"))

# Get a 4-byte little-endian value at virtual location PC of the given ELF.
def data_at(elf, pc):
    for section in elf.iter_sections("SHT_PROGBITS"):
        start = section["sh_addr"]
        end   = section["sh_addr"] + section["sh_size"]

        if start <= pc < end:
            read = section.data()[pc-start : pc+4-start] + bytes(4)
            return read[0] | (read[1] << 8) | (read[2] << 16) | (read[3] << 24)

# Execute a single-fault injection on a binary.
#   command:    QEMU command that runs the binary
#   fault:      Fault description as a string ("s32,1", "s&r32", etc)
#   elf:        ELF used in the QEMU command
#   pc:         Location of the injection
#   block_wall: Whether to enable the block wall after the fault
#
# If a program returns after an injection, this means either the targeted PC
# was not reached, or the fault was bypassed (and the block wall too if
# enabled). To check whether PC was reached a second execution replacing the
# instruction at PC with an illegal instruction is needed. Executions that exit
# normally are slow so doing two is a bit costly. Often there are multiple
# non-reached addresses in a row, so expect_not_reached can be set to do the
# second execution first, removing the need to run the first.
def do_fault_at(command, fault, elf, pc, block_wall, expect_not_reached=False):
    faults = [hex(pc) + ":" + fault]
    print(";".join(faults) + "... ", end="")
    predicted = False

    # If we expect to miss the target, try the illegal instruction immediately.
    # This saves on an unneeded double execution.
    if expect_not_reached:
        ill = [hex(pc) + ":ill"]
        res = run_with_faults(command, ill, block_wall)
        if res == Result.EXITED:
            res = Result.NOT_REACHED
            predicted = True
        else:
            res = run_with_faults(command, faults, block_wall)
    else:
        res = run_with_faults(command, faults, block_wall)

    # If the program exits, PC was probably never reached; try again by
    # generating an illegal instruction on that spot.
    if res == Result.EXITED:
        ill = [hex(pc) + ":ill"]
        res = run_with_faults(command, ill, block_wall)
        if res == Result.EXITED:
            res = Result.NOT_REACHED
        elif res != Result.SIGILL:
            print(f"\x1b[31;1m{res} in EXITED retry?! ", end="")

    # If S&R32 leads to a bypass, check if the replacement actually changed the
    # value being decoded.
    elif res == Result.CCS_BYPASSED and fault == "s&r32":
            original = data_at(elf, pc)
            replaced = data_at(elf, pc - 4)
            assert original is not None
            assert replaced is not None
            if original == replaced:
                res = Result.SILENT_REPLACE

    if res == Result.EXITED or res == Result.CCS_BYPASSED:
        print(f"\x1b[31;1m{res.name}\x1b[0m", end="")
    elif isinstance(res, tuple):
        print(f"\x1b[32m{res[0].name} ({repr(res[1])})\x1b[0m", end="")
    else:
        print(f"\x1b[32m{res.name}\x1b[0m", end="")

    if predicted:
        print(" (predicted)", end="")
    print("")

    return res

def generate_random_faults(offset):
    faults = []
    pc = offset
    maxfaults = random.randint(2, 6)
    n = 0

    while pc < offset + 64 and n < maxfaults:
        x = random.randint(0, 2)
        if x == 0:
            faults.append((pc, "s32,1"))
            pc += 8
        elif x == 1:
            faults.append((pc, "s32,2"))
            pc += 12
        elif x == 2:
            faults.append((pc, "s&r32"))
            pc += 4
        n += 1
        pc += random.randint(0, 6) * 4

    return faults

# Execute a multi-fault injection.
def do_faults(command, faults, elf, block_wall):
    faultspec = [f"{pc:#x}:{f}" for pc, f in faults]
    print(";".join(faultspec) + "... ", end="")
    res = run_with_faults(command, faultspec, block_wall)

    # If the program exits, PC was probably never reached; try again by
    # generating an illegal instruction on that spot.
    if res == Result.EXITED:
        faultspec = [f"{pc:#x}:ill" for pc, _ in faults]
        res = run_with_faults(command, faultspec, block_wall)
        if res == Result.EXITED:
            res = Result.NOT_REACHED
        elif res != Result.SIGILL:
            print(f"\x1b[31;1m{res} in EXITED retry?! ", end="")

    if res == Result.EXITED or res == Result.CCS_BYPASSED:
        print(f"\x1b[31;1m{res.name}\x1b[0m")
    elif isinstance(res, tuple):
        print(f"\x1b[32m{res[0].name} ({repr(res[1])})\x1b[0m")
    else:
        print(f"\x1b[32m{res.name}\x1b[0m")

    return res

class Options:
    pass

def parse_args(argv):
    i = 0
    opt = Options()
    opt.campaign = None
    opt.block_wall = None
    opt.out = None
    opt.name = None
    opt.qemu = None

    try:
        opts, args = getopt.getopt(argv[1:], "d:n:",
            ["campaign=", "qemu=", "block-wall"])
    except getopt.GetoptError as e:
        print("error:", e, file=sys.stderr)
        sys.exit(1)

    for (name, value) in opts:
        if name == "--campaign":
            opt.campaign = value
        if name == "--block-wall":
            opt.block_wall = True
        if name == "-d":
            opt.out = os.path.abspath(value)
        if name == "-n":
            opt.name = value
        if name == "--qemu":
            opt.qemu = os.path.abspath(value)

    if "--help" in argv or len(argv) == 1 or len(args) != 1:
        print(usage.strip())
        sys.exit("--help" not in argv)

    if opt.campaign is None:
        print("error: please specify a campaign with --campaign")
        sys.exit(1)
    if opt.out is None or opt.name is None:
        print("error: please specify an output folder/name with -d and -n")
        sys.exit(1)
    if opt.block_wall is None:
        opt.block_wall = opt.campaign.startswith("ref/")

    return opt, args[0]


def file_notreached(out, name, campaign):
    ckind, _ = split_campaign(campaign)
    return os.path.join(out, name + "-" + ckind + "-notreached.txt")
def file_campaign(out, name, campaign):
    return os.path.join(out, name + "-campaign-" + campaign + ".txt")

def split_campaign(campaign):
    i = campaign.index("-")
    return campaign[:i], campaign[i+1:]

def faultcmd(progdir, campaign):
    ckind, _ = split_campaign(campaign)
    cmd = ["make", "--no-print-directory", f"faultcmd_{ckind.upper()}"]
    rc = subprocess.run(cmd, cwd=progdir, stdout=subprocess.PIPE, check=True)
    out = rc.stdout.decode("utf-8").strip()
    # Still need to silence more directory announcements...
    out = "\n".join(filter(lambda l: not l.startswith("make["),
        out.splitlines()))
    return out

def stat_or_none(path):
    try:
        return os.stat(path)
    except FileNotFoundError:
        return None

def main(argv):
    opt, progdir = parse_args(argv)
    # TODO: Check opt.campaign is a valid campaign name
    qemu = opt.qemu or "qemu-riscv32"

    out_campaign = os.path.abspath(
        file_campaign(opt.out, opt.name, opt.campaign))
    out_notreached = os.path.abspath(
        file_notreached(opt.out, opt.name, opt.campaign))

    command = [qemu] + shlex.split(faultcmd(progdir, opt.campaign))
    print("-> COMMAND:", command)
    print("-> BINARY:", command[1])

    # Switch to the program's directory
    os.chdir(progdir)

    fp = open(command[1], "rb")
    elf = elftools.elf.elffile.ELFFile(fp)

    # Determine the address range to attack
    if opt.campaign.startswith("fsh-"):
        symtable = elf.get_section_by_name(".symtab")
        s = symtable.get_symbol_by_name("__ccs_start")[0].entry["st_value"]
        e = symtable.get_symbol_by_name("__ccs_end")[0].entry["st_value"]
        inj_start, inj_end = s, e
    elif opt.campaign.startswith("ref-"):
        symtable = elf.get_section_by_name(".symtab")
        s = symtable.get_symbol_by_name("__user_start")[0].entry["st_value"]
        e = symtable.get_symbol_by_name("__user_end")[0].entry["st_value"]
        inj_start, inj_end = s, e
    print(f"-> RANGE: {inj_start:#08x} -- {inj_end:#08x}")

    assert run_with_faults(command, [], True) == Result.EXITED

    # Delete the not-reached file if it's older than the program
    stat_nr = stat_or_none(out_notreached)
    stat_elf = os.stat(command[1])
    if stat_nr and stat_elf.st_mtime > stat_nr.st_mtime:
        os.remove(out_notreached)

    # Load the not-reached file if it's there
    not_reached = set()
    try:
        with open(out_notreached, "r") as fp:
            not_reached = set(map(int, fp.read().split(", ")))
    except:
        pass

    ts = TestSet(opt.campaign, inj_start, inj_end)
    ckind, cname = split_campaign(opt.campaign)
    ts.set_report_bypasses(ckind != "ref")
    ts.set_autosave(out_campaign)

    # Load the existing test set if it's not newer than the program
    stat_ts = stat_or_none(out_campaign)
    if stat_ts and stat_ts.st_mtime >= stat_elf.st_mtime:
        with open(out_campaign, "r") as fp:
            ts.load_from_file(fp)

    if ts.finished:
        print("Nothing to do.")
        return 0

    # Save every SAVE_FREQ injections
    SAVE_FREQ = 50
    save_counter = 0

    if cname.startswith("ex-"):
        fault = {
            "ex-s32-1": "s32,1",
            "ex-s32-2": "s32,2",
            "ex-sar32": "s&r32",
        }[cname]

        expect_not_reached = False
        total = len(range(ts.start, ts.end+3, 4))

        for i in range(total):
            addr = ts.start + 4*i
            if ts.next is not None and ts.next > addr:
                continue

            if addr in not_reached:
                r = Result.NOT_REACHED
                expect_not_reached = False
            else:
                print("[{} {:4.1f}%] ".format(opt.name, 100 * i / total),
                    end="")
                r = do_fault_at(command, fault, elf, addr, opt.block_wall,
                    expect_not_reached)
                if r == Result.NOT_REACHED:
                    not_reached.add(addr)
                    # Expect a few consecutive not-reached cases
                    expect_not_reached = True
                else:
                    expect_not_reached = False

            ts.next = addr + 4
            ts.register(r, (addr, fault))

    else:
        # Use a "hash" of the filename as a seed for consistency
        seed = sum(map(ord, command[1]))
        print("-> SEED:", seed)
        random.seed(seed)

        for i in range(2000):
            offset = random.randint(ts.start, ts.end) & -4
            faults = generate_random_faults(offset)
            if ts.next is not None and ts.next > i:
                continue
            print(f"[{opt.name} {i:04d}] ", end="")
            r = do_faults(command, faults, elf, opt.block_wall)
            ts.next = i+1
            ts.register(r, faults)

    fp.close()

    with open(out_campaign, "w") as fp:
        ts.print_to_file(fp, True)
    with open(out_notreached, "w") as fp:
        fp.write(", ".join(map(str, not_reached)))

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
