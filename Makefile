help:
	@echo "Main commands for the Fetch Skips Hardening project:"
	@echo ""
	@echo "all_REF:    Build reference RISC-V programs (no FSH)"
	@echo "run_REF:    Run reference programs to get reference outputs"
	@echo "all_FSH:    Build protected RISC-V programs (with FSH)"
	@echo "run_FSH:    Run FSH programs and compare with reference output"
	@echo "clean:      Clean up build products and binaries"
	@echo ""
	@echo "campaigns:  Run fault injection campaigns (use -jN)"
	@echo "distclean:  Clean up binaries and campaign results (!)"

all_REF run_REF all_FSH run_FSH clean: %:
	@ $(MAKE) -C mibench/automotive/basicmath  $*
	@ $(MAKE) -C mibench/automotive/bitcount   $*
	@ $(MAKE) -C mibench/automotive/qsort      $*
	@ $(MAKE) -C mibench/automotive/susan      $*
	@ $(MAKE) -C mibench/network/dijkstra      $*
	@ $(MAKE) -C mibench/network/patricia      $*
	@ $(MAKE) -C mibench/security/blowfish     $*
	@ $(MAKE) -C mibench/security/rijndael     $*
	@ $(MAKE) -C mibench/security/sha          $*

distclean: clean
	@ rm -rf out/

.PHONY: all_REF run_REF all_FSH run_FSH clean distclean

OUT := out/campaigns

CAMPAIGNS_REF := ref-ex-s32-1 ref-ex-s32-2 ref-ex-sar32
CAMPAIGNS_FSH := fsh-ex-s32-1 fsh-ex-s32-2 fsh-ex-sar32 fsh-multi-random

PROGRAMS := \
  mibench/automotive/basicmath \
  mibench/automotive/bitcount \
  mibench/automotive/qsort \
  mibench/automotive/susan \
  mibench/network/dijkstra \
  mibench/network/patricia \
  mibench/security/blowfish \
  mibench/security/rijndael \
  mibench/security/sha

# Remember the full path of each program x in variable $(PATH_x)
$(foreach P,$(PROGRAMS),$(eval PATH_$(notdir $P) := $P))

# Generate a campaign rule for a single program
#   $1: Program name
#   $2: Campaign name
#   $3: Category-specific (REF/FSH) arguments
# TODO: Avoid the .PHONY
define campaign_rule
$(OUT)/$1-campaign-$2.txt: | $(OUT)/
	./fault.py --campaign=$2 $3 -d "$(OUT)" -n $1 \
	  --qemu="./riscv_qemu_FSH" "$(PATH_$1)"
.PHONY: $(OUT)/$1-campaign-$2.txt
endef

# Generate all campaign rules for a given program
#   $1: Program name (the notdir)
define prog_rules
$(foreach C,$(CAMPAIGNS_REF),
  $(call campaign_rule,$1,$C,--block-wall))
$(foreach C,$(CAMPAIGNS_FSH),
  $(call campaign_rule,$1,$C,))
endef

$(foreach P,$(PROGRAMS),$(eval $(call prog_rules,$(notdir $P))))

# Generate a rule for running a single campaign on all programs
#   $1: Campaign name
#   $2: Campaign-specific arguments
define do_campaign
campaign-$1: $(foreach P,$(PROGRAMS),$(OUT)/$(notdir $P)-campaign-$1.txt)
endef

# Intermediate targets campaigns-* (interesting because within each of these
# targets the campaigns for each program can run in parallel)
$(foreach C,$(CAMPAIGNS_FSH),$(eval $(call do_campaign,$C,)))
$(foreach C,$(CAMPAIGNS_REF),$(eval $(call do_campaign,$C,--block-wall)))

# Multi-core-friendly rules: run campaigns sequentially. We don't want multiple
# campaigns on the same program simultaneously, due to (1) file access races,
# and (2) suboptimal use of not-reached info.
campaigns:
	$(MAKE) campaign-fsh-ex-s32-1
	$(MAKE) campaign-fsh-ex-s32-2
	$(MAKE) campaign-fsh-ex-sar32
	$(MAKE) campaign-fsh-multi-random
	$(MAKE) campaign-ref-ex-s32-1
	$(MAKE) campaign-ref-ex-s32-2
	$(MAKE) campaign-ref-ex-sar32

$(OUT)/:
	@ mkdir -p $@

simulations:

# Generate a simulation rule
#   $1: Program name
#   $2: Cache setting ("nocache" or "icache")
#   $3: Program kind ("REF" or "FSH")
# TODO: Avoid the .PHONY
define simulation_rule
out/m5out/$1_$2_$3:
	@ mkdir -p out/m5out
	./prefix/bin/gem5.opt -r -d "$$@" ./gem5script.py "$$@"

.PHONY: out/m5out/$1_$2_$3
simulations: out/m5out/$1_$2_$3

endef

# Generate simulation rules for all programs
$(foreach P,$(PROGRAMS),$(eval \
  $(call simulation_rule,$(notdir $P),nocache,REF) \
  $(call simulation_rule,$(notdir $P),nocache,FSH) \
  $(call simulation_rule,$(notdir $P),icache,REF) \
  $(call simulation_rule,$(notdir $P),icache,FSH)))

# Final rule for making plots
plots:
	./summary.py
	./plot_campaigns.py
	./plot_performance.py

.PHONY: campaigns% simulations plots
.PRECIOUS: $(OUT)/
