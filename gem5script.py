# From tutorial:
#   <https://www.gem5.org/documentation/learning_gem5/part1/simple_config/>
# with basically no changes

import m5
import sys
from m5.objects import *

# Because most of our programs are small, use a 4 * 2kB instruction cache
# (associativity will matter because dual .text/.text_ccs sections with each
# their own alignment)
class ICache(Cache):
    size = "8kB"
    assoc = 4
    tag_latency = 2
    data_latency = 2
    response_latency = 2
    mshrs = 4
    tgts_per_mshr = 20

    def connectCPU(self, cpu):
        self.cpu_side = cpu.icache_port
    def connectBus(self, bus):
        self.mem_side = bus.cpu_side_ports

class DCache(Cache):
    size = "32kB"
    assoc = 4
    tag_latency = 2
    data_latency = 2
    response_latency = 2
    mshrs = 4
    tgts_per_mshr = 20

    def connectCPU(self, cpu):
        self.cpu_side = cpu.dcache_port
    def connectBus(self, bus):
        self.mem_side = bus.cpu_side_ports

def run(binary, cmd, cache=False):
    system = System()

    # 1 GHz source clock
    system.clk_domain = SrcClockDomain()
    system.clk_domain.clock = "1GHz"
    system.clk_domain.voltage_domain = VoltageDomain()

    # 512 MB of RAM
    system.mem_mode = "timing"
    system.mem_ranges = [AddrRange("512MB")]

    # Basic RISC-V timings
    system.cpu = RiscvTimingSimpleCPU()

    system.membus = SystemXBar()

    # Apply cache as requested
    if cache:
        system.cpu.icache = ICache()
        system.cpu.icache.connectCPU(system.cpu)
        system.cpu.icache.connectBus(system.membus)
        system.cpu.dcache = DCache()
        system.cpu.dcache.connectCPU(system.cpu)
        system.cpu.dcache.connectBus(system.membus)
        # system.cpu.dcache_port = system.membus.cpu_side_ports
    else:
        system.cpu.icache_port = system.membus.cpu_side_ports
        system.cpu.dcache_port = system.membus.cpu_side_ports

    # Trivial interrupt controller
    system.cpu.createInterruptController()
    system.system_port = system.membus.cpu_side_ports

    # DDR3 RAM controller to service RAM requests for entire address range
    system.mem_ctrl = MemCtrl()
    system.mem_ctrl.dram = DDR3_1600_8x8()
    system.mem_ctrl.dram.range = system.mem_ranges[0]
    system.mem_ctrl.port = system.membus.mem_side_ports

    # Emulate a binary file in Syscall Emulation (SE) mode
    system.workload = SEWorkload.init_compatible(binary)

    # Assign threads
    process = Process()
    process.cmd = cmd
    system.cpu.workload = process
    system.cpu.createThreads()

    # Root the system and instantiate objects in the C++ backend
    root = Root(full_system=False, system=system)
    m5.instantiate()

    # Run the simulation

    print("Beginning simulation!")
    exit_event = m5.simulate()

    print('Exiting @ tick {} because {}'
        .format(m5.curTick(), exit_event.getCause()))

EXECUTABLES = {
    "basicmath": "mibench/automotive/basicmath/basicmath_small",
    "bitcount":  "mibench/automotive/bitcount/bitcnts",
    "qsort":     "mibench/automotive/qsort/qsort_small",
    "susan":     "mibench/automotive/susan/susan",
    "dijkstra":  "mibench/network/dijkstra/dijkstra_small",
#    "patricia":  "mibench/network/patricia/patricia",
    "blowfish":  "mibench/security/blowfish/bf",
    "rijndael":  "mibench/security/rijndael/rijndael",
    "sha":       "mibench/security/sha/sha",
}

BLOWFISH_KEY = "1234567890abcdeffedcba0987654321"
AES_KEY = "1234567890abcdeffedcba09876543211234567890abcdeffedcba0987654321"

CLI_ARGS = {
    "basicmath": [],
    "bitcount":  ["10000"],
    "qsort":     ["mibench/automotive/qsort/input_small.dat"],
    "susan":     ["mibench/automotive/susan/input_small.pgm",
                  "out/m5out/susan_output_small.pgm",
                  "-s"],
    "dijkstra":  ["mibench/network/dijkstra/input.dat"],
    "patricia":  ["mibench/network/patricia/small.udp"],
    "blowfish":  ["e",
                  "mibench/security/blowfish/input_small.asc",
                  "out/m5out/blowfish_output_small.enc",
                  BLOWFISH_KEY],
    "rijndael":  ["mibench/security/rijndael/input_small.asc",
                  "out/m5out/rijndael_output_small.enc",
                  "e",
                  AES_KEY],
    "sha":       ["mibench/security/sha/input_small.asc"],
}

def main(output):
    # Output is of the form ".../{name}_{config}_{ctgy}"
    outfolder = os.path.dirname(output)
    name, config, ctgy = os.path.basename(output).split("_")
    assert ctgy == "REF" or ctgy == "FSH"
    cache = (config == "icache") # the other option is "nocache"

    if name not in EXECUTABLES:
        print(f"skipping {name}")
        return 0

    exe = EXECUTABLES[name] + "_" + ctgy
    print("[command line]", exe, CLI_ARGS[name])
    run(exe, [exe] + CLI_ARGS[name], cache)
    return 0

main(*sys.argv[1:])
