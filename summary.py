#! /usr/bin/env python3

from datetime import datetime
import elftools.elf.elffile
import subprocess
import glob
import re
import os

# List of programs as (folder, reference binary, hardened binary) triplets
ALL_PROGRAMS = [
    ("mibench/automotive/basicmath",
        "basicmath_small_REF", "basicmath_small_FSH"),
    ("mibench/automotive/bitcount",
        "bitcnts_REF", "bitcnts_FSH"),
    ("mibench/automotive/qsort",
        "qsort_small_REF", "qsort_small_FSH"),
    ("mibench/automotive/susan",
        "susan_REF", "susan_FSH"),
    ("mibench/network/dijkstra",
        "dijkstra_small_REF", "dijkstra_small_FSH"),
    ("mibench/network/patricia",
        "patricia_REF", "patricia_FSH"),
    ("mibench/security/blowfish",
        "bf_REF", "bf_FSH"),
    ("mibench/security/rijndael",
        "rijndael_REF", "rijndael_FSH"),
    ("mibench/security/sha",
        "sha_REF", "sha_FSH"),
]
# List of campaigns
ALL_CAMPAIGNS = [
    "fsh-ex-s32-1",
    "fsh-ex-s32-2",
    "fsh-ex-sar32",
    "fsh-multi-random",
    "ref-ex-s32-1",
    "ref-ex-s32-2",
    "ref-ex-sar32",
]
# Work folder
WORK_FOLDER = "out/"
# Header for the auto-generated README file
README_HEADER = """
------------------------------
GENERATED FILES - DO NOT EDIT!
------------------------------

These are fetch skips hardening test results.
Generated by {USER}@{HOSTNAME} on {NOW}.

campaigns/*.txt -- generated by fault.py:
  Raw results of fault injection campaigns (might be partial), and information
  about PCs not reached by test executions (*-notreached.txt).
m5out/* -- generated by gem5script.py running in Gem5:
  Raw results of performance simulations in Gem5.

campaigns.csv -- generated by summary.py:
  Summary/statistics of above campaigns in CSV format.
perf.csv -- generated by summary.py:
  Execution time statistics extracted from m5out/ traces, in CSV format.
size.csv -- generated by summary.py:
  Variation in program size between reference and hardened version.

campaigns.png -- generated by plot_campaigns.py:
  Plot of campaigns.csv.
perf.png -- generated by plot_performance.py:
  Plot of size.csv and perf.csv.
""".strip()

# Get the short name for a program given by index
def program_name(i):
    return os.path.basename(ALL_PROGRAMS[i][0])

# Get the campaign output name for a given program by index
def file_campaign_output(i, campaign):
    basename = f"{program_name(i)}-campaign-{campaign}.txt"
    return os.path.join(WORK_FOLDER, "campaigns", basename)

def get_text_and_text_ccs_size(path):
    fp = open(path, "rb")
    elf = elftools.elf.elffile.ELFFile(fp)

    text = elf.get_section_by_name(".text").data_size
    text_ccs = elf.get_section_by_name(".text_ccs")
    text_ccs = 0 if text_ccs is None else text_ccs.data_size

    return (text, text_ccs)

def fault_log_to_csv(path):
    with open(path, "r") as fp:
        progress, header, values, *_ = fp.read().split("\n", 4)

    if progress[0] != "=":
        print(f"{path} is not finished, ignoring")
        return None

    csv = dict(zip(header.split(","), values.split(",")))
    del csv["setting"]
    return csv

def m5out_get_finalTick(folder):
    RE_FINALTICK = re.compile(r"^finalTick\s+(\d+)\s+#")

    with open(os.path.join(folder, "stats.txt"), "r") as fp:
        lines = fp.read().splitlines()
    for l in lines:
        m = RE_FINALTICK.match(l)
        if m:
            return int(m[1])

def main():
    ###-- Generate a summary of fault injection campaigns ---###

    # Read fields from all campaign result files
    results = dict()
    for i in range(len(ALL_PROGRAMS)):
        name = program_name(i)
        for campaign in ALL_CAMPAIGNS:
            csv = fault_log_to_csv(file_campaign_output(i, campaign))
            if csv is not None:
                results[(name, campaign)] = csv

    # Full set of fields found in CSV files
    all_fields = set.union(*[set(csv.keys()) for _, csv in results.items()])
    all_fields = sorted(all_fields)

    # Generate a single large table
    with open(os.path.join(WORK_FOLDER, "campaigns.csv"), "w") as fp:
        fp.write("program,campaign," + ",".join(all_fields) + "\n")
        for (name, campaign), values in results.items():
            fp.write(name + "," + campaign + ",")
            fp.write(",".join(values.get(f, "0") for f in all_fields))
            fp.write("\n")

    # Generate information about file sizes
    with open(os.path.join(WORK_FOLDER, "size.csv"), "w") as fp:
        fp.write("program,size_REF,size_FSH\n")
        for i, (path, refBinary, fshBinary) in enumerate(ALL_PROGRAMS):
            refBinary = os.path.join(path, refBinary)
            fshBinary = os.path.join(path, fshBinary)
            REF_text, _ = get_text_and_text_ccs_size(refBinary)
            FSH_text, FSH_text_ccs = get_text_and_text_ccs_size(fshBinary)
            x = REF_text - FSH_text
            y = FSH_text_ccs
            fp.write(f"{program_name(i)},{x},{y}\n")

    ###--- Generate a summary of performance simulations ---###

    all_m5out = glob.glob("out/m5out/*_*_*/")
    re_m5out_folder = re.compile(r"([^_]+)_([^_]+_[^_]+)")

    tick_values = {}
    for f in all_m5out:
        m = re_m5out_folder.fullmatch(os.path.basename(f[:-1]))
        if not m:
            raise Exception(f"ehrrrr what is {f} supposed to be?")
        name, ctgy = m[1], m[2]
        finalTick = m5out_get_finalTick(f)
        if finalTick is None:
            continue
        if name not in tick_values:
            tick_values[name] = dict()
        tick_values[name][ctgy] = finalTick

    ctgys = sorted({ k for name in tick_values for k in tick_values[name] })
    with open(os.path.join(WORK_FOLDER, "perf.csv"), "w") as fp:
        fp.write("program," + ",".join(ctgys) + "\n")
        for p, res in tick_values.items():
            fields = [str(res.get(ctgy, 0)) for ctgy in ctgys]
            fp.write(p + "," + ",".join(fields) + "\n")

    ###--- Generate a README file with some context ---###

    with open(WORK_FOLDER + "/README", "w") as fp:
        fp.write(README_HEADER.format(
            USER=os.getenv("USER"),
            HOSTNAME=os.uname().nodename,
            NOW=str(datetime.now())) + "\n")

        fp.write("\nTool versions:\n")
        repos = ["llvm-property-preserving", "binutils-gdb", "qemu", "gem5"]
        for repo in repos:
            cmd = ["git", "-C", repo, "rev-parse", "@"]
            proc = subprocess.run(cmd,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            if proc.returncode == 0:
                fp.write("- " + repo + ": " + proc.stdout.decode("utf-8"))
            else:
                fp.write("- " + repo + ": (could not obtain commit)\n")
                print(f"note: could not find commit for {repo}")

        fp.write("\nInput files:\n")
        def writemtime(fp, file):
            dt = datetime.fromtimestamp(os.path.getmtime(file))
            fp.write("- " + file + ": " + str(dt) + "\n")

        for path, refBinary, fshBinary in ALL_PROGRAMS:
            writemtime(fp, os.path.join(path, refBinary))
            writemtime(fp, os.path.join(path, fshBinary))

        for i in range(len(ALL_PROGRAMS)):
            for campaign in ALL_CAMPAIGNS:
                co = file_campaign_output(i, campaign)
                writemtime(fp, co)

main()
