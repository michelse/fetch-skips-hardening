#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    CGO plotting stuff
"""
__author__ = "Laure Gonnord"
__copyright__ = "Grenoble INP/Esisar, 2023"

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import shutil

def compute_derived_metrics(d):
    d["size_ratios"] = d["size_FSH"] / d["size_REF"]
    d["nocache_increase"] = (d["nocache_FSH"] - d["nocache_REF"]) * 100 / d["nocache_REF"]
    d["icache_increase"] = (d["icache_FSH"] - d["icache_REF"]) * 100 / d["icache_REF"]

def plot(dataset, programs):
    plt.rc("axes", axisbelow=True)

    # Use LaTeX if available, but don't require it
    if shutil.which("tex") is not None:
        plt.rcParams["text.usetex"] = True
        plt.rcParams["font.family"] = "Times"
        plt.rcParams["font.size"] = 13

    fig, ax = plt.subplots(1, 3)

    # Add a red line at size ratio 1 for reference
    ax[0].axhline(y=1, color="red", linestyle='-', alpha=0.7, linewidth=1) \
         .set_zorder(1)

    metrics = [
        'Protected code size ratio\n(libraries/runtime not counted)',
        'Execution time overhead in Gem5 in \%\n(without cache)',
        'Execution time overhead in Gem5 in \%\n(8 kB 4-way instruction cache)']
    columns = ['size_ratios', 'nocache_increase', 'icache_increase']

    for i, (metric, column) in enumerate(zip(metrics, columns)):
        if i == 0:
            ax[i].bar(programs, dataset[column], width=0.3, color="tab:gray")
        else:
            ax[i].bar(programs, dataset[column], width=0.3, color="#a986bc")

        blu = ax[i].get_xticklabels()
        ax[i].spines['right'].set_visible(False)
        ax[i].spines['top'].set_visible(False)
        ax[i].set_zorder(0)
        ax[i].yaxis.set_major_locator(
            mpl.ticker.MultipleLocator(0.5 if i == 0 else 10))
        ax[i].set_xticks(ax[i].get_xticks())
        ax[i].set_xticklabels(blu, rotation=55, ha='right', fontsize=13)
        ax[i].set_xlabel(metric, ha='center', rotation=0)
        ax[i].yaxis.grid(color='gray', linestyle='dashed')

    fig.set_size_inches(12, 2)
    fig.savefig("out/perf.png", dpi=400, bbox_inches="tight")

def main():
    # Start with a dataset indexed by programs, i.e program -> program's data
    dataset = dict()

    with open("out/perf.csv", "r") as fp:
        fields = next(fp).strip().split(",")[1:]
        for row in fp:
            program, *values = row.strip().split(",")
            dataset[program] = list(map(int, values))

    with open("out/size.csv", "r") as fp:
        fields += next(fp).strip().split(",")[1:]
        for row in fp:
            program, *values = row.strip().split(",")
            if program in dataset:
                dataset[program] += list(map(int, values))

    # Sort programs by name and transpose to field -> per-program values
    programs = sorted(dataset)
    dataset = {f: np.array([dataset[p][fields.index(f)] for p in programs])
               for f in fields}

    compute_derived_metrics(dataset)
    plot(dataset, programs)

if __name__ == "__main__":
    main()
