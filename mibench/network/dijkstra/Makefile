FILE1 := dijkstra_small.c
FILE2 := dijkstra_large.c
CFLAGS := -Wno-implicit-function-declaration -Wno-return-type

# Build rules

all: all_NAT all_REF all_FSH
all_REF all_FSH all_NAT: all_%: dijkstra_small_% dijkstra_large_%

dijkstra_small_NAT: ${FILE1}
	clang $(CFLAGS) -static -O3 $^ -o $@
dijkstra_large_NAT: ${FILE2}
	clang $(CFLAGS) -static -O3 $^ -o $@

dijkstra_small_REF: ${FILE1}
	../../../riscv_cc_REF $(CFLAGS) -static -O3 $^ -o $@
dijkstra_large_REF: ${FILE2}
	../../../riscv_cc_REF $(CFLAGS) -static -O3 $^ -o $@

dijkstra_small_FSH: ${FILE1}
	../../../riscv_cc_FSH $(CFLAGS) -static -O3 $^ -o $@
dijkstra_large_FSH: ${FILE2}
	../../../riscv_cc_FSH $(CFLAGS) -static -O3 $^ -o $@

# Test rules
# Use riscv_qemu_FSH for REF because it has open() enabled

run: run_NAT run_REF run_FSH
run_NAT run_REF run_FSH: run_%: output_small_%.txt output_large_%.txt

output_small_NAT.txt: dijkstra_small_NAT
	./$< input.dat > $@
output_large_NAT.txt: dijkstra_large_NAT
	./$< input.dat > $@

output_small_REF.txt: dijkstra_small_REF
	../../../riscv_qemu_FSH $< input.dat > $@
output_large_REF.txt: dijkstra_large_REF
	../../../riscv_qemu_FSH $< input.dat > $@

output_small_FSH.txt: dijkstra_small_FSH output_small_REF.txt
	../../../riscv_qemu_FSH $< input.dat > $@
	diff -q output_small_REF.txt $@
output_large_FSH.txt: dijkstra_large_FSH output_large_REF.txt
	../../../riscv_qemu_FSH $< input.dat > $@
	diff -q output_large_REF.txt $@

# Injection rules

faultcmd_REF faultcmd_FSH: faultcmd_%: dijkstra_small_%
	@ echo ./$< input.dat

# Other rules

clean:
	rm -f *.o *NAT *REF *FSH output*
distclean: clean

.PHONY: all% run% clean faultcmd%
