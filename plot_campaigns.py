#! /usr/bin/env python3

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import shutil

PLOT_CAMPAIGNS = [
    "fsh-ex-s32-1", "fsh-ex-s32-2", "fsh-ex-sar32", "fsh-multi-random"
]
PLOT_CAMPAIGN_LABELS = [
    "1", "2", "3", "R",
]
COLUMNS = [
    ("Fault reported",
        "#6eaa56", ["CCS_VIOLATION", "SIGTRAP"]),
    ("Segfault",
        "#e0c050", ["SIGSEGV"]),
    ("Other crash",
        "#a986bc", ["SILENT_REPLACE", "SIGILL", "OTHER"]),
    ("Countermeasure bypassed",
        "#d04030", ["EXITED", "CCS_BYPASSED"]),
]
COLUMN_NAMES = [c[0] for c in COLUMNS]

# Group individual fields into categories
def categorize(dataset, fields):
    # Check that all fields are used in categorization
    assert all(any(f in mapping for (_, _, mapping) in COLUMNS)
               for f in fields if f != "NOT_REACHED")

    categorizedDataset = dict()

    for key, values in dataset.items():
        # Exclude NOT_REACHED from the count
        totalFaults = sum(values) - values[fields.index("NOT_REACHED")]
        categorizedDataset[key] = {"TOTAL": totalFaults}

        for cat, _, mapping in COLUMNS:
            categorizedDataset[key][cat] = \
                sum(values[fields.index(m)] for m in mapping) / totalFaults

    return categorizedDataset

# Group/transpose campaigns by program, using numpy arrays
def group(dataset, programs, campaigns):
    groupedDataset = dict()

    for p in programs:
        groups = dict()
        for cat in {"TOTAL"}.union(COLUMN_NAMES):
            groups[cat] = np.array([dataset[(p, c)][cat] for c in campaigns])

        groupedDataset[p] = groups

    return groupedDataset

def plot(dataset):
    N = len(dataset)
    programs = sorted(dataset)
    groups = list((np.array([4*[u] for u in programs])).flat)

    # Use LaTeX if available, but don't require it
    if shutil.which("tex") is not None:
        plt.rcParams["text.usetex"] = True
        plt.rcParams["font.family"] = "Times"
        plt.rcParams["font.size"] = 13

    fig, ax = plt.subplots(1, N, sharey='all')
    width = N*5/9

    for i, program in enumerate(programs):
        bottom = np.zeros(len(dataset[program]["TOTAL"]))
        for c, color, _ in COLUMNS:
            values = dataset[program][c]
            ax[i].bar(PLOT_CAMPAIGN_LABELS, dataset[program][c],
                      width=0.3, color=color,
                      bottom=bottom, alpha=0.8, edgecolor="black")
            bottom += values

        ax[i].spines["right"].set_visible(False)
        ax[i].spines["top"].set_visible(False)

        # Remove ticks and frame except for the first plot on the left
        if i == 0:
            ax[i].yaxis.set_major_locator(mpl.ticker.MultipleLocator(0.2))
            ax[i].yaxis.set_minor_locator(mpl.ticker.NullLocator())
        else:
            ax[i].spines["left"].set_visible(False)
            ax[i].yaxis.set_ticks_position("none")

        totalFaults = sum(dataset[program]["TOTAL"])
        ax[i].set_xlabel(f"{program}\n({totalFaults} faults)", ha="center")

    fig.legend(COLUMN_NAMES, title="Fault injection outcomes, by proportion",
               loc="lower center", bbox_to_anchor=(0.5, 0.85), frameon=False,
               ncol=4)
    fig.set_size_inches(12, 2)
    fig.savefig("out/campaigns.png", dpi=400, transparent=False,
                bbox_inches="tight")

def main():
    dataset = dict()
    programs = set()

    with open("out/campaigns.csv", "r") as fp:
        fields = next(fp).strip().split(",")[2:]
        for row in fp:
            program, campaign, *values = row.strip().split(",")
            dataset[(program, campaign)] = list(map(int, values))
            programs.add(program)

    dataset = categorize(dataset, fields)
    dataset = group(dataset, programs, PLOT_CAMPAIGNS)
    plot(dataset)

if __name__ == "__main__":
    main()
