FROM ubuntu:22.04 AS layered
ENV DEBIAN_FRONTEND=noninteractive

RUN apt -y update && apt -y upgrade && apt -y install \
      tar wget \
      # LLVM dependencies
      cmake ninja-build lld \
      # binutils dependencies (texinfo shouldn't be needed but I'd rather
      # install it than debug why make rebuilds some .texi file)
      libgmp-dev libmpfr-dev texinfo flex bison \
      # QEMU dependencies
      meson libglib2.0-dev \
      # gem5 dependencies
      build-essential git m4 scons zlib1g zlib1g-dev python3-dev pkg-config \
      libhdf5-serial-dev python3-pydot libpng-dev libelf-dev pip black

# gem5 dependencies
RUN pip install mypy pre-commit pyelftools matplotlib numpy

WORKDIR /root

# Copy source code of the tools we're going to build
COPY llvm-property-preserving llvm-property-preserving/
COPY binutils-gdb binutils-gdb/
COPY gem5 gem5/
# For qemu, copy and extract the release tarball
COPY qemu.tar .
RUN mkdir qemu && cd qemu && tar -xf ../qemu.tar && rm ../qemu.tar

# Build LLVM
RUN mkdir llvm-property-preserving/build && \
    cd llvm-property-preserving/build && \
    cmake -G Ninja \
      -DLLVM_ENABLE_PROJECTS="clang;lldb" \
      -DLLVM_TARGETS_TO_BUILD="RISCV" \
      -DCMAKE_INSTALL_PREFIX="../../prefix" \
      -DCMAKE_BUILD_TYPE=Release \
      -DLLVM_USE_LINKER=lld \
      -DBUILD_SHARED_LIBS=ON \
      -DLLVM_PARALLEL_LINK_JOBS=1 \
      ../llvm && \
    ninja install && \
    cd ../.. && \
    rm -rf llvm-property-preserving

# Download the RISC-V GNU toolchain and strip the binaries for space
RUN wget "https://github.com/riscv-collab/riscv-gnu-toolchain/releases/download/2023.01.31/riscv32-elf-ubuntu-22.04-nightly-2023.01.31-nightly.tar.gz" && \
    tar -xzf "riscv32-elf-ubuntu-22.04-nightly-2023.01.31-nightly.tar.gz" && \
    (strip riscv/bin/*; true) && \
    (strip riscv/libexec/gcc/riscv32-unknown-elf/12.2.0/*; true) && \
    mv riscv riscv-custom && \
    rm "riscv32-elf-ubuntu-22.04-nightly-2023.01.31-nightly.tar.gz"

# Build binutils
RUN mkdir binutils-gdb/build && \
    cd binutils-gdb/build && \
    ../configure \
      --prefix="$(realpath ../../riscv-custom)" \
      --target="riscv32-unknown-elf" && \
    make -j$(nproc) && \
    make install && \
    cd ../.. && \
    rm -rf binutils-gdb

# Build QEMU
RUN mkdir qemu/build && \
    cd qemu/build && \
    ../configure \
      --prefix="$(realpath ../../prefix)" \
      --target-list=riscv32-linux-user \
      --with-git-submodules=ignore && \
    ninja install && \
    cd ../.. && \
    rm -rf qemu

# Build gem5 and copy the executable manually (it's self-contained)
RUN cd gem5 && \
    pip install -r requirements.txt && \
    scons build/RISCV/gem5.opt -j$(nproc) && \
    mv build/RISCV/gem5.opt ~/prefix/bin/ && \
    cd .. && \
    rm -rf gem5

# Copy test files
COPY mibench mibench/
COPY riscv_cc_REF riscv_cc_FSH riscv_qemu_FSH \
     elf32lriscv_ref.x elf32lriscv_ccs.x \
     fault.py summary.py gem5script.py \
     plot_campaigns.py plot_performance.py Makefile README.md .

# Copy reference outputs
COPY out-reference out-reference/

# Squash the final image so we don't ship source and build files as diffs
FROM scratch
COPY --from=layered / /
WORKDIR /root
CMD ["/bin/bash"]
